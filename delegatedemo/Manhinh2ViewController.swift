//
//  Manhinh2ViewController.swift
//  delegatedemo
//
//  Created by TL sama on 2/9/20.
//  Copyright © 2020 TL sama. All rights reserved.
//

import UIKit
protocol Manhinh2ViewControllerProtocol: NSObjectProtocol {
    func yesButtonTapped(_ text: String)
    func noButtonTapped(_ text: String)
}
class Manhinh2ViewController: UIViewController {
    weak var delegate:Manhinh2ViewControllerProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func yesButton(_ sender: Any) {
        delegate?.yesButtonTapped("YES")
    }
    
    @IBAction func noButton(_ sender: Any) {
        delegate?.noButtonTapped("NO")
    }
    
}
