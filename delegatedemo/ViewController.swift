//
//  ViewController.swift
//  delegatedemo
//
//  Created by TL sama on 2/9/20.
//  Copyright © 2020 TL sama. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var txtLabel: UILabel!
    let VC2_IDENTIFIER = "vc2"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    @IBAction func showManhhinh2(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: VC2_IDENTIFIER) as! Manhinh2ViewController
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
extension ViewController:Manhinh2ViewControllerProtocol{
    func yesButtonTapped(_ text: String) {
        txtLabel.text = text
        self.navigationController?.popViewController(animated: true)
    }
    
    func noButtonTapped(_ text: String) {
        txtLabel.text = text
        self.navigationController?.popViewController(animated: true)
    }
}
